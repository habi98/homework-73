const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;

const app = express();
const port = 8001;


app.get('/encode/:text', (req, res) => {
   const encode = Vigenere.Cipher('password').crypt(req.params.text);
   res.send(encode)
});


app.get('/dencode/:text', (req, res) => {
    const dencode = Vigenere.Decipher('password').crypt(req.params.text);
    res.send(dencode)
});

app.listen(port, () => {
    console.log('We are live on ' + port)
});